import React, { Component } from 'react';
import './App.css';
import Layout from "./Components/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import AddNewDish from "./Components/Dishes/AddNewDish/AddNewDish";
import DishesList from "./Containers/DishesList/DishesList";
import Orders from "./Containers/Orders/Orders";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path='/' exact component={DishesList}/>
          <Route path='/add-dish' exact component={AddNewDish}/>
          <Route path='/orders' exact component={Orders}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;
