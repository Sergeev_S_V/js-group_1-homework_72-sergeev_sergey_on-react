import React, {Component} from 'react';
import Button from "../../UI/Button";
import {connect} from "react-redux";
import {getDishes, sendDish} from "../../../store/actions/dishes";
import './AddNewDish.css';

class AddNewDish extends Component {

  state = {
    title: '',
    price: undefined,
    image: '',
  };

  inputChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value})
  };

  sendDishHandler = event => {
    event.preventDefault();
    const dish = {
      title: this.state.title,
      price: this.state.price,
      image: this.state.image,
    };

    this.props.sendDish(dish);
    this.props.getDishes();
    this.props.history.push('/');
  };

  render() {
    return(
      <div className='Add-New-Dish'>
        <form action="">
          <div>
            <span>Title</span>
            <input name='title' type="text" value={this.state.title}
                   onChange={this.inputChangeHandler}/>
          </div>
          <div>
            <span>Price</span>
            <input name='price' type="number" value={this.state.price}
                   onChange={this.inputChangeHandler}/>
          </div>
          <div>
            <span>Image</span>
            <input name='image' type="url" value={this.state.image}
                   onChange={this.inputChangeHandler}/>
          </div>
          <Button clicked={this.sendDishHandler}>Send</Button>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    sendDish: dish => dispatch(sendDish(dish)),
    getDishes: () => dispatch(getDishes()),
  }
};

export default connect(null, mapDispatchToProps)(AddNewDish);