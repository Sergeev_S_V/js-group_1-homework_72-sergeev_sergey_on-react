import React from 'react';
import Button from "../../UI/Button";
import './Dish.css';

const Dish = props => (
  <div className='Dish' onClick={props.isOrder}>
    <img className='Image' src={props.image} alt=""/>
    <span className='Title'>{props.title} </span>
    <span className='Price'>{props.price} KGS </span>
    <Button>Edit</Button>
    <Button clicked={props.remove}>Delete</Button>
  </div>
);

export default Dish;