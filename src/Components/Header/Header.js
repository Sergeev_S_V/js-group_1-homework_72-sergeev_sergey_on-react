import React, {Fragment} from 'react';
import {NavLink} from "react-router-dom";

const Header = props => (
  <Fragment>
    <header>
      <h4>Turtle Pizza Admin</h4>
      <ul>
        <li><NavLink to='/' exact>Dishes</NavLink></li>
        <li><NavLink to='/orders' exact>Orders</NavLink></li>
      </ul>
    </header>
    <main>{props.children}</main>
  </Fragment>
);

export default Header;