import React, {Component} from 'react';
import Button from "../../Components/UI/Button";
import {connect} from "react-redux";
import {getDishes, removeDishFromAPI} from "../../store/actions/dishes";
import Dish from "../../Components/Dishes/Dish/Dish";
import './DishesList.css';

class DishesList extends Component {

  addNewDish = () => {
    this.props.history.push('/add-dish');
  };

  componentDidMount() {
    this.props.getDishes();
  };

  render() {
    return(
      <div className='Dishes-List'>
        <h4 className='Dishes'>Dishes</h4>
        <Button clicked={this.addNewDish}>Add new Dish</Button>
        {this.props.dishes ? Object.keys(this.props.dishes).map(key => (
          <Dish key={key}
                image={this.props.dishes[key].image}
                title={this.props.dishes[key].title}
                price={this.props.dishes[key].price}
                remove={() => this.props.removeDish(key)}
          />
        )) : null}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    dishes: state.dishes.dishes,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getDishes: () => dispatch(getDishes()),
    removeDish: key => dispatch(removeDishFromAPI(key)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(DishesList);