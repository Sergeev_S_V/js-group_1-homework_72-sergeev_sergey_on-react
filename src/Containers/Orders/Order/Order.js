import React from 'react';
import Button from "../../../Components/UI/Button";
import './Order.css';

const costDelivery = 150;

const Order = props => {
  const total = Object.values(props.order)
    .reduce((acc, item) => {
        acc += item.amount * item.cost;
        return acc;
    }, 0);
   return <div className='Order'>
     {Object.keys(props.order).map((dishKey, i) => {
         return <div key={i} className='Order-Item'>
            <span>{props.dishes[dishKey].title}</span>
            <span>{props.dishes[dishKey].price}</span>
            <span> x {props.order[dishKey].amount} </span>
            <span> {props.order[dishKey].amount * props.dishes[dishKey].price} KGS</span>
         </div>
     })}
     <div className='Order-Total'>
       <span>Order total: {total + costDelivery} KGS</span>
       <span>Delivery {costDelivery}</span>
       <Button clicked={props.clicked}>Complete order</Button>
     </div>
   </div>
};

export default Order;