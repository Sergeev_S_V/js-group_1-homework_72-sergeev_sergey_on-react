import React, {Component} from 'react';
import {connect} from "react-redux";
import Order from "./Order/Order";
import {completeOrder, getOrders} from "../../store/actions/orders";
import {getDishes} from "../../store/actions/dishes";
import './Orders.css';

class Orders extends Component {

  componentDidMount() {
    this.props.getOrders();
    this.props.getDishes();
  }

  render() {
    return(
      <div className='Orders'>
        {this.props.dishes && this.props.orders
          ? Object.keys(this.props.orders)
          .map((key, i) =>
            <Order key={i}
                   clicked={() => this.props.completeOrder(key)}
                   order={this.props.orders[key]}
                   dishes={this.props.dishes}/>)
          : null
        }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    orders: state.orders.orders,
    dishes: state.dishes.dishes,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getOrders: () => dispatch(getOrders()),
    getDishes: () => dispatch(getDishes()),
    completeOrder: key => dispatch(completeOrder(key)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Orders);