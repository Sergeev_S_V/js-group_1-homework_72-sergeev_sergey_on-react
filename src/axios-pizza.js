import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://js-hw-66.firebaseio.com',
});

export default instance;