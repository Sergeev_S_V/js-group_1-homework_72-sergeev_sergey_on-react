import * as actionTypes from '../actions/actionTypes';
import axios from '../../axios-pizza';
import {SUCCESS_RESPONSE_DISHES} from "./actionTypes";

export const sendDish = dish => dispatch => {
  dispatch(request());
  axios.post(`/dishes.json`, dish)
    .then(resp => console.log(resp));
};

export const getDishes = () => dispatch => {
  dispatch(request());
  axios.get(`/dishes.json`)
    .then(resp => resp
        ? dispatch(successResponseDishes(resp.data))
        : null)
};

export const request = () => {
  return {type: actionTypes.REQUEST};
};

export const successResponseDishes = dishes => {
  return {type: SUCCESS_RESPONSE_DISHES, dishes};
};

export const removeDishFromState = (key) => {
  return {type: actionTypes.REMOVE, key};
};

export const removeDishFromAPI = key => dispatch => {
  dispatch(removeDishFromState(key));
  axios.delete(`/dishes/${key}.json`);
};
