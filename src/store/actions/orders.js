import axios from '../../axios-pizza';
import {DELETE_ORDER, SUCCESS_RESPONSE_ORDERS} from "./actionTypes";

export const getOrders = () => dispatch => {
  axios.get('/orders.json')
    .then(resp => {
      resp ? dispatch(successResponseOrders(resp.data)) : null
    });
};

export const successResponseOrders = orders => {
  return {type: SUCCESS_RESPONSE_ORDERS, orders};
};

export const deleteOrder = (key) => {
  return {type: DELETE_ORDER, key};
};

export const completeOrder = key => dispatch => {
  dispatch(deleteOrder(key));
  axios.delete(`/orders/${key}.json`);
};