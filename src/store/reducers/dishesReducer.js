import {REMOVE, SUCCESS_RESPONSE_DISHES} from "../actions/actionTypes";

const initialState = {
  dishes: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS_RESPONSE_DISHES:
      return {...state, dishes: action.dishes};
    case REMOVE:
      const newState = {...state,
        dishes: {...state.dishes}};
      delete newState.dishes[action.key];
      return newState;
    default:
      return state;
  }
};

export default reducer;