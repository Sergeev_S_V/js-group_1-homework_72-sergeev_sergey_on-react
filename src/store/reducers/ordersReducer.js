import {SUCCESS_RESPONSE_ORDERS} from "../actions/actionTypes";
import {DELETE_ORDER} from "../actions/actionTypes";

const initialState = {
  orders: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS_RESPONSE_ORDERS:
      return {...state, orders: action.orders};
    case DELETE_ORDER:
      const orders = {...state.orders};
      delete orders[action.key];
      return {...state, orders};
    default:
      return state;
  }
};

export default reducer;